import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class Item {
    private String nama;
    private int harga;
    private int qty;

    public Item(String nama, int harga, int qty) {
        this.nama = nama;
        this.harga = harga;
        this.qty = qty;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
}

public class PMO {
    static Scanner input = new Scanner(System.in);

    static List<Item> total = new ArrayList<>();

    public static void main(String[] args) {
        menu();
    }

    static void menu() {
        boolean ulang = true;
        do {
            System.out.println("================================");
            System.out.println("Selamat datang di BinarFud");
            System.out.println("================================");
            System.out.println("1. Nasi Goreng \t\t| 15.000");
            System.out.println("2. Mie Goreng \t\t| 13.000");
            System.out.println("3. Nasi + Ayam \t\t| 18.000");
            System.out.println("4. Es Teh Manis \t| 3.000");
            System.out.println("5. Es Jeruk \t\t| 5.000");
            System.out.println("99. Pesan dan Bayar");
            System.out.println("0. Keluar Aplikasi");
            System.out.print("=>");
            int selectMenu = input.nextInt();
            switch (selectMenu) {
                case 1:
                    tambahPesanan("Nasi Goreng", 15000);
                    break;
                case 2:
                    tambahPesanan("Mie Goreng", 13000);
                    break;
                case 3:
                    tambahPesanan("Nasi + Ayam", 18000);
                    break;
                case 4:
                    tambahPesanan("Es Teh Manis", 3000);
                    break;
                case 5:
                    tambahPesanan("Es Jeruk", 5000);
                    break;
                case 99:
                    konfirmasiPembayaran();
                    break;
                case 0:
                    ulang = false;
                    break;
                default:
                    System.out.println("Menu tidak valid!");
            }
        } while (ulang);
    }

    static void tambahPesanan(String name, int harga) {
        System.out.println("================================");
        System.out.println("Berapa pesanan anda");
        System.out.println("================================");
        System.out.println("\n" + name + "\t | " + String.valueOf(harga));
        System.out.println("(input 0 untuk kembali)");
        System.out.print("\nqty => ");
        int qty = input.nextInt();
        if (qty != 0) {
            total.add(new Item(name, harga, qty));
            menu();
        }
    }

    static void konfirmasiPembayaran() {
        int totalHargaItems = 0;
        int totalItem = 0;
        System.out.println("================================");
        System.out.println("Konfirmasi & Pembayaran");
        System.out.println("================================\n");
        for (Item item : total) {
            int totalHargaItem = item.getHarga() * item.getQty();
            totalHargaItems += totalHargaItem;
            totalItem += item.getQty();
            System.out.println(item.getNama() + "\t\t" + item.getQty() + "\t" + item.getHarga());
        }
        System.out.println("-------------------------------------+");
        System.out.println("Total \t\t\t" + totalItem + "\t" + totalHargaItems);
        System.out.println("\n1. Konfirmasi dan Bayar");
        System.out.println("2. Kembali ke menu utama");
        System.out.println("0. Keluar aplikasi");
        System.out.print("\n=> ");
        int selectMenu = input.nextInt();
        switch (selectMenu) {
            case 1:
                try {
                    FileWriter strukWriter = new FileWriter("struk.txt");
                    strukWriter.write("=======================\n");
                    strukWriter.write("BinarFud\n");
                    strukWriter.write("=======================\n\n");
                    strukWriter.write("Terima kasih sudah memesan di BinarFud\n");
                    strukWriter.write("Dibawah ini adalah pesanan anda\n\n");
                    for (Item item : total) {
                        strukWriter.write(item.getNama() + "\t\t" + item.getQty() + "\t" + item.getHarga() + "\n");
                    }
                    strukWriter.write("\n-------------------------------------+ \n\n");
                    strukWriter.write("Total \t\t\t" + totalItem + "\t" + totalHargaItems +"\n");
                    strukWriter.write("Pembayaran : BinarCash\n\n");
                    strukWriter.write("=======================\n");
                    strukWriter.write("Simpan struk ini sebagai bukti pembayaran\n");
                    strukWriter.write("=======================\n");
                    strukWriter.close();
                    System.exit(0);
                } catch (IOException e) {
                    System.out.println("An error occured");
                    e.printStackTrace();
                }
                break;
            case 2:
                menu();
            case 0:
                System.exit(0);
            default:
                menu();
        }
    }
}
